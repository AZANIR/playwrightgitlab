import { Page, test, expect } from "@playwright/test";
import { BasePage } from "./base-page.po";


export class HomePage extends BasePage {
    page: Page;

    constructor(page: Page) {
        super(page);
        this.page = page;
    }

    public async navigateToHomePage() {
        await this.page.goto("https://playwright.dev/");
    }

}