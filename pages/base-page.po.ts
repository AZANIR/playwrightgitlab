import { Page, test, expect } from "@playwright/test";


export abstract class BasePage {
    page: Page;
    constructor(page: Page) {
        this.page = page;
    }
    public async navigateTo(url: string) {
        await this.page.goto(url);
    }

}